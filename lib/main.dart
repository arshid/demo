import 'package:demo/condroller/app_controller.dart';
import 'package:demo/views/home/main_home_holder.dart';
import 'package:demo/views/splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

void main() async {
  // MapController mapCtP = Get.put(MapController());
//  await mapCtP.getUserLocation();
  Get.put(AppController());
  await GetStorage.init();
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<AppController>(builder: (ct) {
      return GetMaterialApp(
        title: 'demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: ct.isAuth
            ? const MainHomeHolder()
            : ct.isFirstTime
                ? FutureBuilder(
                    future: ct.tryAutoLogin(),
                    builder: (context, snapshot) =>
                        snapshot.connectionState == ConnectionState.waiting
                            ? const SplashScreen()
                            : const MainHomeHolder())
                : const MainHomeHolder(),
      );
    });
  }
}
