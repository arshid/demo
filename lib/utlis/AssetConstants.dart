class AssetConstant {
  //Menu Icon
  static const String appName = 'Demo App';
  static const String logo = 'assets/images/logo.png';
  static const String home = 'assets/images/home.svg';
  static const String home_inactive = 'assets/images/home_inactive.svg';

  static const String profile = 'assets/images/profile.svg';
  static const String profile_inactive = 'assets/images/profile_inactive.svg';
  static const String map_pin = 'assets/images/map_pin.svg';
}
