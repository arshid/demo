import 'package:demo/condroller/app_controller.dart';
import 'package:demo/utlis/AppColorCode.dart';
import 'package:demo/utlis/AppFontOswald.dart';
import 'package:demo/utlis/AssetConstants.dart';
import 'package:demo/views/auth/login_screen.dart';
import 'package:demo/views/home/main_home_holder.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  AppController mainCtr = Get.find();
  @override
  void initState() {
    Future.delayed(const Duration(seconds: 1), () {
      if (mainCtr.isAuth) {
        Get.offAll(() => MainHomeHolder());
      } else {
        Get.offAll(() => const LoginScreen());
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      // backgroundColor: AppColorCode.bgColor,
      body: Container(
        width: size.width,
        height: size.height,
        decoration: const BoxDecoration(
          color: AppColorCode.brandColor,
          // image: new DecorationImage(
          //     fit: BoxFit.cover,
          //     colorFilter: ColorFilter.mode(
          //         Colors.black.withOpacity(0.1), BlendMode.dstATop),
          //     image: new AssetImage(
          //       AssetConstant.bgImage,
          //     )),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            //  Image.asset(AssetConstant.logo),
            const SizedBox(height: 14),
            Text(
              AssetConstant.appName,
              style: AppFontMain(
                color: AppColorCode.whiteshadow,
                fontSize: 36,
                fontWeight: FontWeight.w700,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
