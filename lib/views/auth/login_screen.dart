import 'package:demo/condroller/app_controller.dart';
import 'package:demo/utlis/AppColorCode.dart';
import 'package:demo/utlis/AppFontOswald.dart';
import 'package:demo/utlis/AssetConstants.dart';
import 'package:demo/views/auth/register_screen.dart';
import 'package:demo/views/home/main_home_holder.dart';
import 'package:demo/views/widgets/button_icons_widgets.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  AppController mainCtr = Get.find();
  bool passwordVisible = false;
  bool rememberMe = false;
  bool _loading = false;
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  void dispose() {
    emailController.dispose();
    passwordController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SafeArea(
      child: Scaffold(
          body: ListView(
        children: [
          Container(
            width: size.width,
            height: size.height * 0.4,
            color: AppColorCode.brandColor,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                // Image.asset(AssetConstant.logo),
                const SizedBox(height: 14),
                Text(
                  AssetConstant.appName,
                  style: AppFontMain(
                    color: AppColorCode.pureWhite,
                    fontSize: 31,
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(height: 50),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  buildtextForm(
                    label: 'Email',
                    controller: emailController,
                    validator: (val) {
                      if (val!.isEmpty) {
                        return 'Enter Email ID';
                      }
                      if (!RegExp(
                              r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                          .hasMatch(val)) {
                        return 'Enter a Valid Email';
                      }
                      return null;
                    },
                  ),
                  const SizedBox(height: 20),
                  buildtextForm(
                    label: 'Password',
                    controller: passwordController,
                    suffixIcon: InkWell(
                      onTap: () {
                        setState(() {
                          passwordVisible = !passwordVisible;
                        });
                      },
                      child: passwordVisible
                          ? const Icon(
                              Icons.visibility_off,
                              color: AppColorCode.grey2,
                            )
                          : const Icon(
                              Icons.visibility,
                              color: AppColorCode.grey2,
                            ),
                    ),
                    obscureText: passwordVisible ? false : true,
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Enter Password';
                      }

                      return null;
                    },
                  ),
                ],
              ),
            ),
          ),
          const SizedBox(height: 30),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: button(
              label: 'Log In',
              height: size.height * 0.07,
              width: size.width,
              onTap: () async {
                if (_formKey.currentState!.validate()) {
                  var flag = await mainCtr.loginUser(
                      emailController.text, passwordController.text);
                  if (flag) {
                    Get.off(() => const MainHomeHolder());
                  } else {
                    setState(() {
                      _loading = false;
                    });
                    Fluttertoast.showToast(
                        msg: 'Please check email or password ',
                        backgroundColor: AppColorCode.brandColor);
                  }
                }
                //  Get.offAll(() => MainHomeHolder());
              },
            ),
          ),
          const SizedBox(height: 20),
          Center(
            child: InkWell(
              onTap: () {
                Get.to(() => const SignUpScreen());
              },
              child: Text(
                'Register',
                style: AppFontMain(),
              ),
            ),
          ),
          const SizedBox(height: 50),
        ],
      )),
    );
  }
}
