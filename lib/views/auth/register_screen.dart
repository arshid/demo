import 'package:demo/condroller/app_controller.dart';
import 'package:demo/model/user_details.dart';
import 'package:demo/utlis/AppColorCode.dart';
import 'package:demo/utlis/AppFontOswald.dart';
import 'package:demo/utlis/AssetConstants.dart';
import 'package:demo/views/home/main_home_holder.dart';
import 'package:demo/views/widgets/button_icons_widgets.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';

class SignUpScreen extends StatefulWidget {
  const SignUpScreen({Key? key}) : super(key: key);

  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  final _formKey = GlobalKey<FormState>();
  AppController mainCtr = Get.find();
  bool isSaving = false;
  bool _loading = true;
  TextEditingController emailController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confrPasswordCt = TextEditingController();
  bool passwordVisible = false;
  bool verifyPasswordVisible = false;
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SafeArea(
      child: Scaffold(
          body: ListView(
        children: [
          Container(
            width: size.width,
            height: size.height * 0.4,
            color: AppColorCode.brandColor,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                //  Image.asset(AssetConstant.logo),
                const SizedBox(height: 14),
                Text(
                  AssetConstant.appName,
                  style: AppFontMain(
                    color: AppColorCode.pureWhite,
                    fontSize: 31,
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(height: 50),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  buildtextForm(
                    label: 'Name',
                    controller: nameController,
                    validator: (val) {
                      if (val!.isEmpty) return 'Required';
                    },
                  ),
                  const SizedBox(height: 20),
                  buildtextForm(
                    label: 'Email',
                    controller: emailController,
                    validator: (val) {
                      if (val!.isEmpty) {
                        return 'Enter Email ID';
                      }
                      if (!RegExp(
                              r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                          .hasMatch(val)) {
                        return 'Enter a Valid Email';
                      }
                      return null;
                    },
                  ),
                  const SizedBox(height: 20),
                  buildtextForm(
                    label: 'Password',
                    controller: passwordController,
                    suffixIcon: InkWell(
                      onTap: () {
                        setState(() {
                          passwordVisible = !passwordVisible;
                        });
                      },
                      child: passwordVisible
                          ? const Icon(
                              Icons.visibility_off,
                              color: AppColorCode.grey2,
                            )
                          : const Icon(
                              Icons.visibility,
                              color: AppColorCode.grey2,
                            ),
                    ),
                    obscureText: passwordVisible ? false : true,
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Enter Password';
                      }

                      return null;
                    },
                  ),
                  const SizedBox(height: 20),
                  buildtextForm(
                    label: 'Verify Password',
                    controller: confrPasswordCt,
                    suffixIcon: InkWell(
                      onTap: () {
                        setState(() {
                          verifyPasswordVisible = !verifyPasswordVisible;
                        });
                      },
                      child: verifyPasswordVisible
                          ? const Icon(
                              Icons.visibility_off,
                              color: AppColorCode.grey2,
                            )
                          : const Icon(
                              Icons.visibility,
                              color: AppColorCode.grey2,
                            ),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) return 'Confirm your Password';
                      if (value != passwordController.text) {
                        return 'Password must be same as above';
                      }
                      return null;
                    },
                    obscureText: verifyPasswordVisible ? false : true,
                  ),
                ],
              ),
            ),
          ),
          const SizedBox(height: 30),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: button(
              label: 'Next',
              height: size.height * 0.07,
              width: size.width,
              onTap: () async {
                if (_formKey.currentState!.validate()) {
                  setState(() {
                    _loading = true;
                  });

                  final data = AuthList(
                      name: nameController.text,
                      email: emailController.text,
                      password: passwordController.text);
                  var flag = await mainCtr.signUpUser(data);
                  if (flag) {
                    Get.off(() => MainHomeHolder());
                  } else {
                    setState(() {
                      _loading = false;
                    });
                    Fluttertoast.showToast(
                        msg: 'This email address is already being used ',
                        backgroundColor: AppColorCode.brandColor);
                  }
                }
              },
            ),
          ),
          const SizedBox(height: 50),
        ],
      )),
    );
  }
}
