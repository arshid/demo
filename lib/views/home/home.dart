import 'package:demo/utlis/AppColorCode.dart';
import 'package:demo/utlis/AppFontOswald.dart';
import 'package:demo/utlis/AssetConstants.dart';
import 'package:demo/views/widgets/button_icons_widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:location/location.dart';
import 'package:geocoding/geocoding.dart' as geo;

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  Location currentLocation = Location();
  List<geo.Placemark>? placemarks;
  geo.Placemark? places;
  String? placeName;
  String? street;
  bool locationView = false;
  @override
  void initState() {
    super.initState();
    setState(() {});
  }

  void getLocation() async {
    var location = await currentLocation.getLocation();
    //print(location.latitude);
    placemarks = await geo.placemarkFromCoordinates(
        location.latitude ?? 0.0, location.longitude ?? 0.0);
    places = placemarks!.firstWhere((element) => element.name != null);
    // locality = places!.locality;
    // subAdministrativeArea = places!.subAdministrativeArea;
    setState(() {
      placeName = places!.name;
    });
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    Widget titleBar() {
      return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const SizedBox(width: 5),
          InkWell(
            onTap: () {},
            child: Row(
              children: [
                Text(
                  placeName ?? '',
                  style: AppFontMain(
                    color: AppColorCode.mainBlue,
                    fontSize: 12,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                const SizedBox(width: 5),
                SvgPicture.asset(AssetConstant.map_pin),
              ],
            ),
          ),
        ],
      );
    }

    return SafeArea(
        child: Scaffold(
            body: Center(
                child: ListView(children: [
      SizedBox(height: 200),
      locationView
          ? Center(child: titleBar())
          : Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: button(
                  label: 'Display the location',
                  height: size.height * 0.07,
                  width: size.width,
                  onTap: () async {
                    getLocation();
                    if (mounted) {
                      setState(() {
                        locationView = true;
                      });
                    }
                  }),
            ),
      const SizedBox(height: 30),
    ]))));
  }
}
