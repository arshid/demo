import 'package:demo/condroller/app_controller.dart';
import 'package:demo/utlis/AppColorCode.dart';
import 'package:demo/utlis/AppFontOswald.dart';
import 'package:demo/views/auth/login_screen.dart';
import 'package:demo/views/widgets/button_icons_widgets.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ProfilePageScreen extends StatefulWidget {
  const ProfilePageScreen({Key? key}) : super(key: key);

  @override
  _ProfilePageScreenState createState() => _ProfilePageScreenState();
}

class Task {
  final String name;
  final String description;

  Task({required this.name, required this.description});
}

class _ProfilePageScreenState extends State<ProfilePageScreen> {
  AppController mainCtr = Get.find();

  void clearTasks() async {
    // mainCtr.clearTokken();

    Get.offAll(const LoginScreen());
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(
            child: Container(height: 50, width: 50, child: CircleAvatar()),
          ),
          const SizedBox(height: 30),
          Text(
            mainCtr.dataList['name'].toString(),
            style: AppFontMain(
              color: AppColorCode.brandColor,
              fontSize: 14,
              fontWeight: FontWeight.w500,
            ),
          ),
          const SizedBox(height: 30),
          Text(
            mainCtr.dataList['email'].toString(),
            style: AppFontMain(
              color: AppColorCode.brandColor,
              fontSize: 14,
              fontWeight: FontWeight.w500,
            ),
          ),
          const SizedBox(height: 150),
          button(
              label: 'Log Out',
              height: size.height * 0.07,
              width: size.width * 0.20,
              onTap: () async {
                clearTasks();
              }),
        ],
      ),
    );
  }
}
