// import 'package:flutter/material.dart';
// import 'package:get/get.dart';
// import 'package:google_maps_flutter/google_maps_flutter.dart';
// import 'package:location/location.dart' as loc;
// import 'package:geocoding/geocoding.dart' as geo;

// class MapController extends GetxController {
//   Set<Marker> _markers = {};
//   Set<Marker> get markers => _markers;
//   BitmapDescriptor? descriptor;

//   LatLng _latLong = LatLng(25.890677490509702, 75.47032225108443);
//   LatLng get latLong => _latLong;
//   // GetAddressResponse addressData = GetAddressResponse();

//   void setMarker(LatLng latLng) {
//     _markers.clear();
//     _markers.add(
//       Marker(
//           markerId: MarkerId(latLng.toString()),
//           position: latLng,
//           icon: descriptor!,
//           draggable: true,
//           onDragEnd: ((newPosition) {
//             _latLong = newPosition;
//             print(newPosition.latitude);
//             print(newPosition.longitude);
//           })),
//     );
//   }

//   // iconMark() async {
//   //   descriptor = await BitmapDescriptor.fromAssetImage(
//   //       ImageConfiguration.empty, AssetConstant.mapMarker);
//   // }

//   loc.Location location = new loc.Location();
//   loc.LocationData? _locationData;
//   loc.LocationData? get locationData => _locationData;

//   String? locality;
//   String? subAdministrativeArea;
//   String? curLocality;
//   String? curArea;
//   getUserPlace() async {
//     _locationData = await location.getLocation();
//     _latLong = LatLng(_locationData!.latitude!, _locationData!.longitude!);
//     placemarks = await geo.placemarkFromCoordinates(
//         _latLong.latitude, _latLong.longitude);
//     places = placemarks!.firstWhere((element) => element.name != null);
//     locality = places!.locality;
//     subAdministrativeArea = places!.subAdministrativeArea;
//     placeName = places!.name;
//   }

//   getUserLocation() async {
//     _locationData = await location.getLocation();
//     _latLong = LatLng(_locationData!.latitude!, _locationData!.longitude!);
//     await getAddressFromLatLng(_latLong);
//   }

//   getCurentUserLocation() async {
//     _locationData = await location.getLocation();
//     _latLong = LatLng(_locationData!.latitude!, _locationData!.longitude!);
//     getCurentAddressFromLatLng(_latLong);
//   }

//   updateLatLong(LatLng latLng) {
//     _latLong = latLng;
//     print(_latLong);
//   }

//   int? addressType;
//   updateAddressType(int id) {
//     addressType = id;
//   }

//   List<geo.Placemark>? placemarks;
//   geo.Placemark? places;
//   String? placeName;
//   String? street;
//   String? postalCode;
//   String? countryCode;
//   //  void updateLocation(String locatonName, String state, String pin) {
//   //   locatonName = locatonName;
//   //   state = state;
//   //   pin = pin;
//   // }
//   getCurentAddressFromLatLng(LatLng latLng) async {
//     placemarks =
//         await geo.placemarkFromCoordinates(latLng.latitude, latLng.longitude);

//     places = placemarks!.firstWhere((element) => element.name != null);
//     curArea = places!.locality!;
//   }

//   getAddressFromLatLng(LatLng latLng) async {
//     // setMarker(latLng);
//     _latLong = latLng;
//     placemarks =
//         await geo.placemarkFromCoordinates(latLng.latitude, latLng.longitude);
//
//     places = placemarks!.firstWhere((element) => element.name != null);
//     locality = places!.locality!;
//     street = places!.subAdministrativeArea!;
//     postalCode = places!.postalCode!;
//     countryCode = places!.isoCountryCode!;
//     print(locality);
//     update();
//   }
// }
