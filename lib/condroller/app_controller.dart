import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class AppController extends GetxController {
  String? _token;
  bool get isAuth => _token != null;
  bool isFirstTime = true;
  List<dynamic> _auth = [];

  final auth = GetStorage();
  final tokken = GetStorage();
  List storageList = [];
  setFirstTimeUser() async {
    if (isFirstTime) {
      tokken.write('first_time', false);
      //    print('setting first time');
      if (tokken.read('first_time')) {}
      //    print('trueee');
    } else {
      return;
    }
  }

  Future<bool> tryAutoLogin() async {
    await Future.delayed(const Duration(
      seconds: 1,
    ));
    if (tokken.read('tokken') != null) {
      final extractedUserData = tokken.read('tokken');
      getAuthList();
      checkEmail(extractedUserData);
      _token = extractedUserData;

      print(extractedUserData + 'tokken here');
      update();
      return true;
    }
    return false;
  }

  ////////Auth//////////
  void getAuthList() {
    if (auth.read('users') != null) {
      _auth = auth.read('users');
      update();
    }
  }

  dynamic dataList;
  checkEmail(String email) {
    dataList = _auth.firstWhere((element) => element['email'] == email,
        orElse: () => null);
    update();
  }

  Future<bool> signUpUser(dynamic data) async {
    if (data != null) {
      dataList = null;
      getAuthList();
      checkEmail(data.email);
      if (dataList == null) {
        final storageMap = {};
        storageMap['name'] = data.name;
        storageMap['email'] = data.email;
        storageMap['password'] = data.password;
        storageList.add(storageMap);
        auth.write('users', storageList);
        auth.write('tokken', data.email);
        _token = data.email;
        getAuthList();
        checkEmail(data.email);
        update();
        return true;
      }

      return false;
    } else {
      return false;
    }
  }

  Future<bool> loginUser(String email, String password) async {
    getAuthList();
    checkEmail(email);
    if (dataList == null) {
      return false;
    } else {
      if (dataList['password'] == password) {
        return true;
      } else {
        return false;
      }
    }
  }

  void clearTokken() {
    _auth.clear();

    tokken.remove('tokken');
    auth.erase();
  }
}
