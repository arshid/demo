class AuthList {
  final String name;
  final String email;
  final String password;

  AuthList({required this.name, required this.email, required this.password});
}
